extends Area2D

signal just_pressed(id)

var is_near = false
var id 

func set_id(_id):
	id = _id

func _process(delta):
	if is_near and not $Sprite.flip_v and Input.is_action_just_pressed("ui_select"):
		$Sprite.flip_v = true
		emit_signal("just_pressed", id)

func reset():
	$Sprite.flip_v = false;

func _on_Box_body_entered(body):
	is_near = true
	
func _on_Box_body_exited(body):
	is_near = false
