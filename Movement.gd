extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
#func _ready():
#	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var mov = Vector2(
		Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left"), 
		0)
	mov *= 250
	move_and_collide(mov * delta)
