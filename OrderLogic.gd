extends Sprite	

var boxes
var clicked

# Called when the node enters the scene tree for the first time.
func _ready():
	clicked = []
	boxes = get_children()
	for i in range(len(boxes)):
		boxes[i].connect("just_pressed", self, "_on_box_opened") 
		boxes[i].set_id(i)
		boxes[i].reset()


func _on_box_opened(index):
	clicked.append(index)
	print(clicked)
	if len(clicked) == len(boxes):
		if clicked == range(len(boxes)):
			flip_v = true
		else:
			for box in boxes:
				box.reset()
			clicked = []
